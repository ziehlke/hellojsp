<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.temporal.ChronoUnit" %>
<%@ page import="java.time.Duration" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Uptime</title>
</head>
<body>

<jsp:include page="header.jsp"/>

<br>

<%
    application.getAttribute("uptime");
%>

<%! LocalDateTime start = LocalDateTime.now(); %>

<%= start.until(LocalDateTime.now(), ChronoUnit.SECONDS)%>




<%@ include file="footer.jsp" %>
</body>
</html>
