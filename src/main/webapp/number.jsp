<%@ page import="java.util.Random" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Random number</title>
    <link rel="stylesheet" href="static/style.css" type="text/css">
</head>
<body>

<jsp:include page="header.jsp"/>
<br>

<%
    Random random = new Random();
    int nextInt = random.nextInt(99);


    if (nextInt < 50) {
        out.println("<p class=\"blue\">" + nextInt + "</p>");
        out.println("<p> Liczba mniejsza od 50</p>");
        out.println();
        out.println();

    } else {

        out.println("<p class=\"green\">" + nextInt + "</p>");
        out.println("<p>Liczba równa lub większa od 50</p>");
        out.println();
        out.println();
    }
%>
test

<%--Jeśli liczba jest mniejsza od 50 wyświetl na ekranie napis "Liczba mniejsza od 50", a samą liczbę pokoloruj na niebiesko--%>
<%--Jeśli liczba jest równa lub większa 50 wyświetl na ekranie napisz "Liczba równa lub większa od 50", a samą liczbę pokoloruj na zielono--%>

<%@ include file="footer.jsp" %>
</body>
</html>
