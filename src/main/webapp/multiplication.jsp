<%@ page import="java.util.Base64" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="static/style.css" type="text/css">
    <title>Multiplication Table</title>
</head>
<body>

<jsp:include page="header.jsp"/>

<table>
    <%

        int iLimit = 10;
        int jLimit = 10;


        String rowsFromCookie = "";
        String columnsFromCookie = "";
        Cookie[] cookies = request.getCookies();
        for (Cookie c : cookies) {
            switch (c.getName()) {
                case "numberOfRowsCookie":
                    rowsFromCookie = "value=\"" + String.valueOf(Base64.getDecoder().decode(c.getValue())) + "\"";
                    break;

                case "numberOfColumns":
                    columnsFromCookie = "value=\"" + String.valueOf(Base64.getDecoder().decode(c.getValue())) + "\"";
                    break;
                default:
                    break;
            }
        }


        if (request.getParameter("numberOfRows") != null && request.getParameter("numberOfColumns") != null) {

            out.print(request.getParameter("numberOfRows"));
            out.print(request.getParameter("numberOfColumns"));


            iLimit = Integer.valueOf(request.getParameter("numberOfRows"));
            jLimit = Integer.valueOf(request.getParameter("numberOfColumns"));

            String numberOfRowsCookieString = Base64.getEncoder().encodeToString((String.valueOf(iLimit).getBytes());
            String numberOfColumnsCookieString = Base64.getEncoder().encodeToString((String.valueOf(jLimit).getBytes());

            Cookie numberOfRowsCookie = new Cookie("numberOfRowsCookie", numberOfRowsCookieString);
            Cookie numberOfColumnsCookie = new Cookie("numberOfColumnsCookie", numberOfColumnsCookieString);

            response.addCookie(numberOfColumnsCookie);
            response.addCookie(numberOfRowsCookie);
        }


        for (int i = 1; i <= iLimit; i++) {
            out.println("<tr>");

            for (int j = 1; j <= jLimit; j++) {
                String tdClass = "";

                if (i == j) {
                    tdClass = "diagonal";
                } else if (i < j) {
                    tdClass = "upper";
                } else {
                    tdClass = "lower";
                }


                out.println("<td class=\"" + tdClass + "\">" + i * j + "</td>");
            }
            out.println("</tr>");
        }
    %>
</table>


<form action="multiplication.jsp" method="POST">
    <label for="numberOfRows">Podaj ilość wierszy</label>
    <input type="number" id="numberOfRows" name="numberOfRows" value="<%= rowsFromCookie%>"/>
    <br/>
    <label for="numberOfColumns">Podaj ilość kolumn</label>
    <input type="number" id="numberOfColumns" name="numberOfColumns" value="<%= columnsFromCookie%>"/>
    <br/>

    <input type="submit" value="Wyślij"/>


    <%@ include file="footer.jsp" %>
</body>
</html>
